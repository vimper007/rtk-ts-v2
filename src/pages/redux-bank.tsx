import { useSelector } from "react-redux";
import AccountOperations from "../features/account/AccountOperations";
import BalanceDisplay from "../features/account/BalanceDisplay";
import CreateCustomer from "../features/customer/CreateCustomer";
import Customer from "../features/customer/Customer";
import { RootState } from "../store-v2";

const ReduxBank = () => {
  const fullName = useSelector((state: RootState) => state.customer.fullName);
  return (
    <div>
      <h1>🏦 The React-Redux Bank ⚛️</h1>
      {fullName === "" ? (
        <CreateCustomer />
      ) : (
        <>
          <Customer />
          <AccountOperations />
          <BalanceDisplay />
        </>
      )}
    </div>
  );
};

export default ReduxBank;
