import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from 'axios';
import { AppDispatch, RootState } from "../../store";
const base_url = 'http://localhost:9001';

type DataType =
    {
        id: number,
        first_name: string,
        last_name: string,
        email: string,
        gender: string
    }

const initialState: initialStateType = {
    loading: null,
    data: null,
    error: null,
}

type initialStateType = {
    loading: null | string,
    data: null | DataType,
    error: string | null,
}

export const fetchDashboardData = createAsyncThunk<DataType, void, { dispatch: AppDispatch, getState: RootState }>(
    'dashboard/fetchDashboardData',
    async (_, { dispatch, getState }) => {
        const response = await axios.get(`${base_url}/user`);
        getState
        return response.data;
    },
    {
        condition(_: void, { getState }) {
            const { dashboard } = getState() as RootState;
            if (dashboard.loading === 'pending') {
                console.log("dashboard.loading...", dashboard.loading)
            }
            return true
        },
    }
)

const dashboardSlice = createSlice({
    name: 'dashbaord',
    initialState,
    reducers: {

    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchDashboardData.pending, (state) => {
                state.loading = 'pending';
            })
            .addCase(fetchDashboardData.fulfilled, (state, action) => {
                state.loading = 'fulfilled';
                state.data = action.payload;
            })
            .addCase(fetchDashboardData.rejected, (state, action) => {
                state.loading = 'rejected';
                state.error = action.error.message || 'Failed';
                console.log('action', action)
            })
    }
})

export const dashboardReducer = dashboardSlice.reducer