import { useSelector } from "react-redux";
import { RootState } from "../../store-v2";

function formatCurrency(value:any) {
    return new Intl.NumberFormat("en", {
      style: "currency",
      currency: "USD",
    }).format(value);
  }
  
  function BalanceDisplay() {
    const {balance} = useSelector((state:RootState)=>state.account)
    return <div className="balance">{formatCurrency(balance )}</div>;
  }
  
  export default BalanceDisplay;
  