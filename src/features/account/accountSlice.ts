import { ActionCreatorWithPayload, PayloadAction } from "@reduxjs/toolkit";
import { Action, AnyAction, UnknownAction } from "redux";

type InitialStateAccountType = {
    balance: number,
    loan: number,
    purpose: string,
}
type AccountActionType =
    (() => {
        type: "account/deposit";
        payload: number;
    }) | {
        type: "account/deposit";
        payload: number;
    }
    | { type: 'account/withdraw', payload: any }
    | { type: 'account/requestLoan', payload: { amount: number, purpose: string } }
    | { type: 'account/payLoan' };

const initialStateAccount: InitialStateAccountType = {
    balance: 0,
    loan: 0,
    purpose: '',
};

export const accountReducer = (state = initialStateAccount, action: PayloadAction<any>) => {
    switch (action.type) {
        case 'account/deposit':
            return { ...state, balance: state.balance + action.payload };
        case 'account/withdraw':
            return { ...state, balance: state.balance - action.payload };
        case 'account/requestLoan':
            if (state.loan > 0) return state;
            return { ...state, balance: state.balance + action.payload.amount, loan: action.payload.amount, purpose: action.payload.purpose }
        case 'account/payLoan':
            return { ...state, balance: state.balance - state.loan, loan: 0, purpose: '' }
        default:
            return state
    }
}

export const deposit = (amount: number, currency: string) => {
    // if (currency === 'USD') {

        return {
            type: 'account/deposit' as const, payload: amount
        }
    // }
    // return function () {
    //     return { type: 'account/deposit' as const, payload: amount }
    // }
}


export const withdraw = (amount: number) => {
    return {
        type: 'account/withdraw' as const, payload: amount
    }
}

export const requestLoan = (amount: number, purpose: string) => {
    return {
        type: 'account/requestLoan' as const, payload: { amount, purpose }
    }
}

export const payLoan = () => {
    return {
        type: 'account/payLoan' as const
    }
}
