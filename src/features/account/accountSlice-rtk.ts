import { PayloadAction, createSlice } from "@reduxjs/toolkit";

type InitialStateAccountType = {
    balance: number,
    loan: number,
    purpose: string,
    isLoading: boolean,
}

const initialStateAccount: InitialStateAccountType = {
    balance: 0,
    loan: 0,
    purpose: '',
    isLoading: false
};

export const accountSlice = createSlice({
    name: 'account',
    initialState: initialStateAccount,
    reducers: {
        deposit: (state, action: PayloadAction<{ amount: number, currency: string }>) => {
            state.balance += action.payload.amount;
            state.isLoading = false;
        },
        withdraw: (state, action: PayloadAction<number>) => {
            state.balance -= action.payload;
        },
        requestLoan: {
            prepare(amount, purpose) {
                return {
                    payload: { amount, purpose }
                }
            },
            reducer: (state, action: PayloadAction<{ amount: number, purpose: string }>) => {
                state.balance += + action.payload.amount;
                state.loan += action.payload.amount;
                state.purpose = action.payload.purpose
            },
        },
        payLoan: (state) => {
            state.balance -= state.loan;
            state.loan = 0;
            state.purpose = ''
        },
        convertingCurrency: (state) => {
            state.isLoading = true
        }
    }
})
export const { payLoan, requestLoan, withdraw } = accountSlice.actions;

export const deposit = (amount: number, currency: string) => {
    // if (currency === 'USD') {
        return { type: 'account/deposit', payload: { amount, currency } }
    // } 
    // else {
    //     return async function (dispatch: any, getState: InitialStateAccountType) {
    //         dispatch({ type: 'account/convertingCurrency' });
    //         const host = 'api.frankfurter.app';
    //         const res = await fetch(`https://${host}/latest?amount=${amount}&from=${currency}&to=USD`)
    //         const data = await res.json();
    //         const converted = data.rates.USD;

    //         dispatch({ type: 'account/deposit', payload: converted })
    //     }
    // }
}

export default accountSlice.reducer;
