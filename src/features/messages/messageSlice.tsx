import { createSlice } from "@reduxjs/toolkit";

type MessageState = string;

const initialState: MessageState = 'txt'

const MessageSlice = createSlice({
    initialState,
    name: 'message',
    reducers: {
        display: (state) => {
           return state + '1'
        }
    }
})

console.log(MessageSlice.actions.display())

export const { display } = MessageSlice.actions;
export const MessageReducer = MessageSlice.reducer;