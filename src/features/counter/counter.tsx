import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import { CounterSlice, increment } from './counterSlice'
import { display } from '../messages/messageSlice'
type Props = {}

const counter = (props: Props) => {
  const value = useSelector((store:RootState)=>store.counter.value)
  const message = useSelector((store:RootState)=>store.message)
  const dispatch = useDispatch();
  const newState = CounterSlice.reducer({value:11},{type:'counter/increment'});
  console.log("news",newState)
  return (
    <div>
      <h2>Counter</h2>
      <p>value: {value}</p>
      <button onClick={()=>dispatch(increment())}>Increment</button>
      <hr />
      <h2>{message}</h2>
      <button onClick={()=>dispatch(display())}>Change Text</button>
    </div>
  )
}

export default counter