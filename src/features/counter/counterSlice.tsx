import { createSlice } from "@reduxjs/toolkit";

type CounterState = {
    value: number
}

const initialState: CounterState = {
    value: 0
}

export const CounterSlice = createSlice({
    initialState,
    name: "counter1",
    reducers: {
        increment: (state) => {
            state.value++;
        }
    }
})

export const { increment } = CounterSlice.actions;
export const CounterReducer = CounterSlice.reducer;
