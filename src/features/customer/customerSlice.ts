import { PayloadAction, createSlice } from "@reduxjs/toolkit";

type initialStateCustomerType = {
    fullName: string,
    nationalID: string,
    createdAt: string,
}

const initialStateCustomer: initialStateCustomerType = {
    fullName: '',
    nationalID: '',
    createdAt: '',
};

type CustomerActionType =
    | {
        type: 'customer/createCustomer',
        payload: {
            fullName: string,
            nationalID: string,
            createdAt: Date
        }
    }
    | {
        type: 'customer/updateName',
        payload: {
            fullName: string,
        }
    }

export const customerReducer = (state = initialStateCustomer, action: PayloadAction<initialStateCustomerType>) => {
    switch (action.type) {
        case 'customer/createCustomer':
            return {
                ...state,
                fullName: action.payload.fullName,
                nationalID: action.payload.nationalID,
                createdAt: action.payload.createdAt
            }
        case 'customer/updateName':
            return {
                ...state,
                fullName: action.payload.fullName
            }
        default:
            return state
    }
}

export const customerSlice = createSlice({
    name: 'customer',
    initialState: initialStateCustomer,
    reducers: {
        createCustomer: (state, action) => {
            state.fullName = action.payload.fullName,
                state.nationalID = action.payload.nationalID,
                state.createdAt = action.payload.createdAt
        },
        updateName: (state, action) => {
            state.fullName = action.payload.fullName;
        }
    }
})

export const customerReducerV2 = customerSlice.reducer;



export const createCustomer = (fullName: string, nationalID: string, createdAt: Date) => {
    return {
        type: 'customer/createCustomer',
        payload: {
            fullName,
            nationalID,
            createdAt
        }
    }
}

export const updateName = (fullName: string) => {
    return {
        type: 'customer/updateName',
        payload: fullName
    }
}
