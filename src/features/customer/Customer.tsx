import { useSelector } from "react-redux";
import {RootState} from '../../store-v2'
function Customer() {
  const customer = useSelector((state:RootState)=>state.customer.fullName);
  return <h2>👋 Welcome, {customer}</h2>;
}

export default Customer;
