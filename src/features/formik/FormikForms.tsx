import { Container, FormLabel, Grid, Typography } from "@mui/material"
import { Form, Formik } from "formik"
import * as Yup from 'yup'
import TextFieldWrapper from "../../components/formsUI/textField"
import SelectFieldWrapper from "../../components/formsUI/selectField"
import Countries from '../../server/countries.json';

type Props = {}

const initialFormState = {
    firstName:'',
    lastName:'',
    email:'',
    phone:'',
    addressLine1:'',
    addressLine2:'',
    city:'',
    stateDistrictProvince:'',
    country:''
}

const formValidation = Yup.object().shape({
    firstName:Yup.string().required('Required'),
    lastName: Yup.string().required('Required'),
    email: Yup.string().email('Invalid Email').required('Required'),
    phone: Yup.number().integer().typeError('Invalid phone number').required('Required'),
    addressLine1:Yup.string().required('Required'),
    addressLine2:Yup.string(),
    city:Yup.string().required('Required'),
    stateDistrictProvince:Yup.string().required('Required'),
    country:Yup.string().required('Required')
})

const FormikForms = (props: Props) => {
    return (
        <Grid container >
            <Grid item xs={12}>
                Header
            </Grid>
            <Grid item xs={12}>
                <Container maxWidth='md'>
                    <div className="form-wrapper">
                        <Formik
                            initialValues={initialFormState}
                            onSubmit={(values) => {
                                console.log('values', values)
                            }}
                            validationSchema={formValidation}
                        >
                            <Form >
                                <Grid container spacing={2}>

                                    <Grid item xs={12}>
                                        <Typography>
                                            Your Details
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextFieldWrapper name="firstName" label='First Name' color="secondary"/>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextFieldWrapper name="lastName" label='Last Name'/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextFieldWrapper name="email" label='Email' color="secondary"/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextFieldWrapper name="phone" label='Phone'/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography>
                                            Address
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextFieldWrapper name="addressLine1" label='Address Line 1'/>
                                    </Grid><Grid item xs={12}>
                                        <TextFieldWrapper name="addressLine2" label='Address Line 2'/>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextFieldWrapper name="city" label='City'/>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextFieldWrapper name="stateDistrictProvince" label='State/District/Province'/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <SelectFieldWrapper name="country" options={Countries} />
                                    </Grid>
                                    
                                    <Grid item xs={12}>
                                        <Typography>
                                            Booking Information
                                        </Typography>
                                    </Grid>

                                </Grid>
                            </Form>
                        </Formik>
                    </div>
                </Container>
            </Grid>
        </Grid>
    )
}

export default FormikForms