import { TableOptions, useReactTable } from '@tanstack/react-table'
import { useState } from 'react'

type User = {
    name: {
        first: string
        last: string
    }
    info: {
        age: number
        visits: number
    }
}

//note: data needs a "stable" reference in order to prevent infinite re-renders
//or
const [data, setData] = useState<User[]>([
    {
        "name": {
            "first": "Tanner",
            "last": "Linsley"
        },
        "info": {
            "age": 33,
            "visits": 100,
        }
    },
    {
        "name": {
            "first": "Kevin",
            "last": "Vandy"
        },
        "info": {
            "age": 27,
            "visits": 200,
        }
    }
])


const columns = [
    {
      header: 'First Name',
      accessorKey: 'name.first',
    },
    {
      header: 'Last Name',
      accessorKey: 'name.last',
    },
    {
      header: 'Age',
      accessorFn: (info:User['info']) => info.age, 
    },
  ]


const UsersTable = () => {
    // const table = useReactTable(data)
    return (
        <div>UsersTable</div>
    )
}

export default UsersTable