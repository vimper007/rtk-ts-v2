import { useState } from "react"
import { useForm } from 'react-hook-form'
type Props = {}

const Users = (props: Props) => {
    const [input, setInput] = useState('');
    const { register, formState: { errors }, handleSubmit } = useForm<{ email: string }>({mode:'onTouched'});
    const onSubmit = () => {
        console.log('submit')
    }
    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>
            <input type="text" {...register('email', {
                required: { value: true, message: "Email Required" },
                maxLength: { value: 10, message: 'Too many characters' },
            })} placeholder="Email" />
            <p>{errors.email?.message}</p>
            </form>
        </div>
    )
}

export default Users