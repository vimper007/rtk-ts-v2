import { useSelector } from 'react-redux'
import { RootState } from '../../app/store'
import { Link } from 'react-router-dom'
import { PostAuthor } from './PostAuthor'

export const PostsList = () => {
  const posts = useSelector((state:RootState) => state.posts)

  const renderedPosts = posts.map(post => (
    <Link to={`/postlists/${post.id}`} key={post.id}>
    <article className="post-excerpt" key={post.id}>
      <h3>{post.title}</h3>
      <p className="post-content">{post.content.substring(0, 100)}</p>
      <PostAuthor userId={post.user} />
    </article>
    </Link>
  ))

  return (
    <section className="posts-list">
      <h2>Posts</h2>
      {renderedPosts}
    </section>
  )
}