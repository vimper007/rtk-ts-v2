import { useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom'
import { RootState } from '../../app/store';
import { PostAuthor } from './PostAuthor';

const SinglePostPage = () => {
    const { postId } = useParams<string>();
    const post = useSelector((state: RootState) => state.posts.find(post => post.id === postId))
    return (
        <div>
            <article className="post-excerpt" key={post?.id}>
                <h3>{post?.title}</h3>
                <p className="post-content">{post?.content}</p>
                <PostAuthor userId={post?.user} />
            </article>
            <Link to={`/editPost/${post?.id}`} className="button">
                Edit Post
            </Link>
        </div>
    )
}

export default SinglePostPage