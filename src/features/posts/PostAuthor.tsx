import { useSelector } from 'react-redux'
import { RootState } from '../../app/store'

type Props = {
    userId: string | undefined
}

export const PostAuthor = ({ userId }: Props) => {
    const author = useSelector((state: RootState) =>
        state.users.find(user => user.id === userId)
    )

    return <span>by {author ? author.name : 'Unknown author'}</span>
}