import { ChangeEvent, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { postAdded } from './postSlice'
import { RootState } from '../../app/store'

export const AddPostForm = () => {
    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const [user, setUser] = useState('0')

    const onTitleChanged = (e: ChangeEvent<HTMLInputElement>) => setTitle(e.target.value)
    const onContentChanged = (e: ChangeEvent<HTMLTextAreaElement>) => setContent(e.target.value)
    const dispatch = useDispatch();
    const users = useSelector((state: RootState) => state.users)

    const onSavePostClicked = () => {
        if (title && content) {
            dispatch(postAdded(title, content, user))
            setTitle('')
            setContent('')
        }
    }

    const selectAuthor = (e: ChangeEvent<HTMLSelectElement>) => {
        setUser(e.target.value)
    }

    const userOptions = users.map(option => {
        return <option value={option.id} key={option.id}>{option.name}</option>
    })

    return (
        <section>
            <h2>Add a New Post</h2>
            <form>
                <label htmlFor="postTitle">Post Title:</label>
                <input
                    type="text"
                    id="postTitle"
                    name="postTitle"
                    value={title}
                    onChange={onTitleChanged}
                />
                <label htmlFor="postContent">Content:</label>
                <textarea
                    id="postContent"
                    name="postContent"
                    value={content}
                    onChange={onContentChanged}
                />
                <label htmlFor="postAuthor">Author:</label>
                <select name="user" id="postAuthor" onChange={selectAuthor}>
                    {userOptions}
                </select>
                <button type="button" onClick={onSavePostClicked}>Save Post</button>
            </form>
        </section>
    )
}