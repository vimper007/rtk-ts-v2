import { formatDistanceToNow, parseISO } from 'date-fns';

type Props = {
    timestamp: string,
}

const TimeAgo = ({ timestamp }: Props) => {
    let timeAgo = '';
    if (timestamp) {
        const date = parseISO(timestamp);
        const timeperiod = formatDistanceToNow(date);
        console.log('date---', date);
        timeAgo = `${timeperiod} ago`;
    }
    return (
        <span title={timestamp}>
            &nbsp; <i>{timeAgo}</i>
        </span>
    )
}

export default TimeAgo