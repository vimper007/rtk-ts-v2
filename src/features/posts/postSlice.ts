import { PayloadAction, createSlice, nanoid } from "@reduxjs/toolkit";

type PostState = {
  id: string;
  title: string;
  content: string;
  user: string;
};

const initialState: PostState[] = [
  { id: "1", title: "First Post!", content: "Hello!", user: '1' },
  { id: "2", title: "Second Post", content: "More text", user: '2' },
];

export const postSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    // postAdded: {
    //   reducer(state, action:PayloadAction<PostState>) {
    //     state.push(action.payload);
    //   },
    //   prepare(title: string, content: string) {
    //     return {
    //       payload: {
    //         id: nanoid(),
    //         title,
    //         content
    //       }
    //     }
    //   }
    // },
    postAdded: {
      prepare(title: string, content: string, userId) {
        console.log(new Date().toISOString())
        return {
          payload: {
            id: nanoid(),
            date:new Date().toISOString(),
            title,
            content,
            user: userId,
          }
        }
      },
      reducer(state, action: PayloadAction<PostState>) {
        state.push(action.payload)
      }
    },
    postUpdated(state, action) {
      const { id, title, content } = action.payload
      const existingPost = state.find(post => post.id === id)
      if (existingPost) {
        existingPost.title = title
        existingPost.content = content
      }
    }
  },
});

export const PostReducer = postSlice.reducer;
export const { postAdded, postUpdated } = postSlice.actions;
           