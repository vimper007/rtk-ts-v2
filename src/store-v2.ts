import { Action, applyMiddleware, combineReducers, configureStore, createStore } from "@reduxjs/toolkit";
import { customerReducer } from "./features/customer/customerSlice";
import { accountReducer } from "./features/account/accountSlice";
import { ThunkAction, thunk } from "redux-thunk";
import { useDispatch } from "react-redux";
import { AppDispatch } from "./store";

const rootReducer = combineReducers({
    account: accountReducer,
    customer: customerReducer,
})

const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
    devTools:true,
})
// const store = createStore(rootReducer,applyMiddleware(thunk))
export default store;
export type RootState = ReturnType<typeof store.getState>
