import { NavLink, Route, Router, Routes } from "react-router-dom";
import "./App.css";
import Counter from "./features/counter/counter";
import { PostsList } from "./features/posts/PostsList";
import { AddPostForm } from "./features/posts/AddPostForm";
import SinglePostPage from "./features/posts/SinglePostPage";
import { EditPostForm } from "./features/posts/EditPostForm";
import FormikForms from "./features/formik/FormikForms";
import Users from "./features/users/components/users";
import "./store-v2";
import ReduxBank from "./pages/redux-bank";
import Dashboard from "./features/dashboard/Dashboard";
function App() {
  return (
    <>
      <NavLink to="counter">Counter</NavLink>
      <NavLink to="postlists">Post Lists</NavLink>
      <NavLink to="formik">Formik</NavLink>
      <NavLink to="react-hook-form">React Hook Form</NavLink>
      <NavLink to="redux-bank">Redux Bank</NavLink>
      <NavLink to="dashboard">Dashboard</NavLink>

      <Routes>
        <Route path="counter" element={<Counter />} />
        <Route
          path="postlists"
          element={
            <>
              <AddPostForm />
              <PostsList />
            </>
          }
        />
        <Route path="/postlists/:postId" element={<SinglePostPage />} />
        <Route path="/editPost/:postId" element={<EditPostForm />} />
        <Route path="/formik" element={<FormikForms />} />
        <Route path="/react-hook-form" element={<Users />} />
        <Route path="/redux-bank" element={<ReduxBank />} />
        <Route path="/dashboard" element={<Dashboard />} />
      </Routes>
    </>
  );
}

export default App;
