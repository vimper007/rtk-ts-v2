import { configureStore } from "@reduxjs/toolkit";
import accountReducer from './features/account/accountSlice-rtk'
import { useDispatch } from "react-redux";
import { dashboardReducer } from "./features/dashboard/dashboardSlice";
import { customerReducerV2 } from "./features/customer/customerSlice";


export const store = configureStore({
    reducer:{
        account:accountReducer,
        dashboard:dashboardReducer,
        customer:customerReducerV2
    },
    devTools:true,
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch = useDispatch.withTypes<AppDispatch>()
export type RootState = ReturnType<typeof store.getState>