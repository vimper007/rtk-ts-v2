import { createStore } from "@reduxjs/toolkit";

type InitialStateType = {
    balance: number,
    loan: number,
    purpose: string,
}
type ActionType =
    | { type: 'account/deposit', payload: number }
    | { type: 'account/withdraw', payload: any }
    | { type: 'account/requestLoan', payload: { amount: number, purpose: string } }
    | { type: 'account/payLoan' }

const initialState: InitialStateType = {
    balance: 0,
    loan: 0,
    purpose: '',
};

const reducer = (state = initialState, action: ActionType) => {
    switch (action.type) {
        case 'account/deposit':
            return { ...state, balance: state.balance + action.payload };
        case 'account/withdraw':
            return { ...state, balance: state.balance - action.payload };
        case 'account/requestLoan':
            if (state.loan > 0) return state;
            return { ...state, balance: state.balance + action.payload.amount, loan: action.payload.amount, purpose: action.payload.purpose }
        case 'account/payLoan':
            return { ...state, balance: state.balance - state.loan, loan: 0, purpose: '' }
    }
}

const store = createStore(reducer);

// store.dispatch({ type: 'account/deposit', payload: 500 })
// console.log(store.getState());
// store.dispatch({ type: 'account/withdraw', payload: 300 });
// console.log(store.getState());
// store.dispatch({ type: 'account/requestLoan', payload: { amount: 1000, purpose: 'Buy a car' } });
// console.log(store.getState());
// store.dispatch({ type: 'account/payLoan' });
// console.log(store.getState());

const deposit = (amount: number) => (

    {
        type: 'account/deposit' as const, payload: amount
    }
)


const withdraw = (amount: number) => {
    return {
        type: 'account/withdraw' as const, payload: amount
    }
}

const requestLoan = (amount: number, purpose: string) => {
    return {
        type: 'account/requestLoan' as const, payload: { amount, purpose }
    }
}

const payLoan = () => {
    return {
        type: 'account/payLoan' as const
    }
}

store.dispatch(deposit(500))
console.log(store.getState());
store.dispatch(withdraw(300));
console.log(store.getState());
store.dispatch(requestLoan(500, 'Buy a cheap car'));
console.log(store.getState());
store.dispatch(payLoan());
console.log(store.getState());