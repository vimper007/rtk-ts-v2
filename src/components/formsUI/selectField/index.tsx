import { MenuItem, TextField, TextFieldProps, TextFieldVariants } from '@mui/material'
import { FieldInputProps, useField } from 'formik'
import React, { ChangeEvent } from 'react'

type Props = TextFieldProps & {
    name: string,
    options: any,
    otherProps?:any
}

type ConfigSelectType = TextFieldProps & {
    select: boolean,
    fullWidth: boolean,
    variant: TextFieldVariants,
    field: FieldInputProps<string>
}

const SelectFieldWrapper = ({ name, options, otherProps }: Props) => {
    const [field, meta, helpers] = useField(name);

    const handleChange = (e: ChangeEvent<any>) => {
        helpers.setValue(e.target.value)
        helpers.setTouched(true);
    }

    const handleClick = () => {
        // if(field.value) return;
        // helpers.setTouched(true)
    }

    const configSelect: ConfigSelectType = {
        select: true,
        fullWidth: true,
        variant: 'outlined',
        ...otherProps,
        ...field,
        onChange: handleChange,
        onClick: handleClick,
    }

    if(meta.touched && meta.error){
        configSelect.helperText=meta.error;
        configSelect.error = true
    }

    return (
        <>
            <TextField {...configSelect} >
                {
                    Object.keys(options).map((item, pos) => {
                        return (
                            <MenuItem key={pos} value={item}>
                                {options[item]}
                            </MenuItem>
                        )
                    })
                }
            </TextField>
        </>
    )
}

export default SelectFieldWrapper