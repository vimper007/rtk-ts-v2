import { TextField, TextFieldProps, TextFieldVariants } from '@mui/material'
import { useField } from 'formik'
import { ReactNode } from 'react'

type Props = {
    name: string,
} & TextFieldProps

type configTextFieldType = {
    fullWidth: boolean,
    variant: TextFieldVariants,
    helperText?: ReactNode,
    error?: boolean,

} & TextFieldProps

const TextFieldWrapper = ({ name, ...otherProps }: Props) => {
    const [field, meta] = useField(name)
    const configTextField: configTextFieldType = {
        ...otherProps,
        ...field,
        fullWidth: true,
        variant: 'outlined'
    }
    if (meta.touched && meta.error) {
        configTextField.helperText = meta.error;
        configTextField.error = true;

    }
    return (
        <TextField {...configTextField} />
    )
}

export default TextFieldWrapper